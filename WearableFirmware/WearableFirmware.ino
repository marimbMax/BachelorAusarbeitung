#include <nRF5x_BLE_API.h>
//#include <BLE_API.h>

#include "Adafruit_TLC59711.h"
#include "Adafruit_BME280.h"
#include "Thread.h"

// original code by Vincent Diener
// code adapted to new shoulder wearable

#define DEVICE_NAME       "TECO Wearable 1"

//activates the board
//must be set to HIGH prior to any any interaction on the spi or i2c bus
//otherwise the tlc and/or the bme can be damaged
#define VCC_ON A3

#define OUTPUTS_PER_BOARD 12

//pins for spi bus - connected to tlc
#define dataPin   A4
#define clockPin  A5

//number of tlcs
//#define NUM_TLC59711 5
#define NUM_TLC59711 8
#define NUM_ACTUATORS 16
#define MAX_FREQUENCY 21

#define WRITE_BUF_LEN 1

//const uint8_t actuator_channels[NUM_ACTUATORS] = {
//  0,            // TLC 1
//  12, 18, 21,   // TLC 2
//  24, 30, 33,   // TLC 3
//  36, 42, 45    // TLC 4
//};

//const uint8_t actuator_channels[NUM_ACTUATORS] = {
//  0, 6, 9,      // TLC 1
//  12, 18, 21,   // TLC 2
//  24, 30, 33,   // TLC 3 Zusatzmotor neben Erde
//  36, 42, 45,   // TLC 4
//  48, 54, 57,   // TLC 5
//  60, 66, 69,   // TLC 6
//  72, 78, 81,   // TLC 7
//  84, 90, 93   // TLC 8
//};

const uint8_t actuator_channels[NUM_ACTUATORS] = {
  6, 9,      // TLC 1
  18, 21,   // TLC 2
  30, 33,   // TLC 3 Zusatzmotor neben Erde
  42, 45,   // TLC 4
  54, 57,   // TLC 5
  66, 69,   // TLC 6
  78, 81,   // TLC 7
  90, 93   // TLC 8
};

//activates debugging on serial port
//will lead to nonstable readings for the bme sensor
//for debugging the bme sensor use a ble connection
#define DEBUG 0

//use software spi for tlc writings
Adafruit_TLC59711 tlc = Adafruit_TLC59711(NUM_TLC59711, clockPin, dataPin);

BLE ble;

uint8_t write_value[WRITE_BUF_LEN] = {0};
uint8_t count_value[1] = {NUM_TLC59711};
uint8_t freq_value[1] = {MAX_FREQUENCY};

Ticker ticker_task1;

static const uint8_t writeService_uuid[]           = {0x71, 0x3D, 0, 0, 0x50, 0x3E, 0x4C, 0x75, 0xBA, 0x94, 0x31, 0x48, 0xF1, 0x8D, 0x94, 0x1E};
static const uint8_t motor_count_uuid[]            = {0x71, 0x3D, 0, 1, 0x50, 0x3E, 0x4C, 0x75, 0xBA, 0x94, 0x31, 0x48, 0xF1, 0x8D, 0x94, 0x1E};
static const uint8_t frequency_uuid[]              = {0x71, 0x3D, 0, 2, 0x50, 0x3E, 0x4C, 0x75, 0xBA, 0x94, 0x31, 0x48, 0xF1, 0x8D, 0x94, 0x1E};
static const uint8_t gatt_write_uuid[]             = {0x71, 0x3D, 0, 3, 0x50, 0x3E, 0x4C, 0x75, 0xBA, 0x94, 0x31, 0x48, 0xF1, 0x8D, 0x94, 0x1E};

uint8_t zero[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8_t current[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
//uint8_t zero[]    = {0x00, 0x00, 0x00, 0x00};
//uint8_t current[] = {0x00, 0x00, 0x00, 0x00};

GattCharacteristic writeCharacteristic(gatt_write_uuid, write_value, 1, WRITE_BUF_LEN, GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_READ | GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_WRITE | GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_WRITE_WITHOUT_RESPONSE);
GattCharacteristic countCharacteristic(motor_count_uuid, count_value, 1, 1, GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_READ);
GattCharacteristic freqCharacteristic(frequency_uuid, freq_value, 1, 1, GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_READ);

GattCharacteristic *allChars[] = {&writeCharacteristic, &countCharacteristic, &freqCharacteristic};
GattService         writeService(writeService_uuid, allChars, sizeof(allChars) / sizeof(GattCharacteristic *));


//ble callbacks
void disconnectionCallBack(const Gap::DisconnectionCallbackParams_t *params) {
  allOff();
  writeWithoutSpinOn(); 
  
  Serial.println("- DISCONNECTED -");
  ble.startAdvertising();
}

void connectionCallBack(const Gap::ConnectionCallbackParams_t *params) { 
  Serial.println("- CONNECTED -");
}


void gattServerWriteCallBack(const GattWriteCallbackParams *Handler) {
  uint8_t buf[WRITE_BUF_LEN];
  uint16_t bytesRead;

  Serial.print("Trying to write... ");
  
  delay(15);
  
  if (Handler->handle == writeCharacteristic.getValueAttribute().getHandle()) {
    ble.readCharacteristicValue(writeCharacteristic.getValueAttribute().getHandle(), buf, &bytesRead);
  
    //for(int i = 0; i < bytesRead; i++){
    //  current[i] = buf[i];
    //  Serial.print("0x");
    //  Serial.print(buf[i], HEX);
    //  Serial.print(" ");
    //}

    if((int)buf[0] == 16) {
      for(int i = 0; i < 16; i++) {
        current[i] = 0x00;  
      }  
    } else {
      for(int i = 0; i < 16; i++) {
        if(i == (int) buf[0]){
          current[i] = 0xFF;
        } else {
          current[i] = 0x00;     
        }
      }
    }
  
    Serial.print("0x");
    Serial.print(buf[0], HEX);
    Serial.print(" ");
    Serial.println(" - Written!");

    writeWithoutSpinOn();
  }
}

void writeWithoutSpinOn() {
  
  for(int i = 0; i < NUM_ACTUATORS; i++){ // NUM_TLC59711 vorher
      uint8_t channel = actuator_channels[i];
      uint16_t duty_cycle = (int) ( (current[i] / 255.0) * 65535 ); // 16 bit PWM => 65536 steps

      for (int output = 0; output < 3; output++) {
        tlc.setPWM(channel + output, duty_cycle);
      }
    }
  
  
  tlc.write(); 
  Serial.println("Updating...");
}

void periodicCallback() {
  if (ble.getGapState().connected) {
    writeWithoutSpinOn();
  }
}




void setup() {
  Serial.begin(9600);
  Serial.println("Vibration Demo");
  
  pinMode(VCC_ON, OUTPUT);
  digitalWrite(VCC_ON, HIGH);
  
  tlc.begin();
  tlc.write();

  //writeWithoutSpinOn(); //test

  //ticker_task1.attach(periodicCallback, 0.1);
  
  ble.init();
  ble.onDisconnection(disconnectionCallBack);
  ble.onConnection(connectionCallBack);
  ble.onDataWritten(gattServerWriteCallBack);
  
  ble.accumulateAdvertisingPayload(GapAdvertisingData::BREDR_NOT_SUPPORTED | GapAdvertisingData::LE_GENERAL_DISCOVERABLE);
  ble.accumulateAdvertisingPayload(GapAdvertisingData::COMPLETE_LOCAL_NAME, (uint8_t *)DEVICE_NAME, sizeof(DEVICE_NAME));
  ble.accumulateAdvertisingPayload(GapAdvertisingData::SHORTENED_LOCAL_NAME, (uint8_t *)DEVICE_NAME, sizeof(DEVICE_NAME));

  ble.addService(writeService);

  ble.setAdvertisingType(GapAdvertisingParams::ADV_CONNECTABLE_UNDIRECTED);

  ble.setDeviceName((const uint8_t *)DEVICE_NAME);
  ble.setTxPower(4);
  ble.setAdvertisingInterval(160);
  ble.setAdvertisingTimeout(0);

  ble.startAdvertising();
}

void loop() {
  ble.waitForEvent();
}

void allOff() {
  for (int i = 0; i < NUM_ACTUATORS; i++){
    current[i] = 0x00;
  }
  
  ble.updateCharacteristicValue(writeCharacteristic.getValueAttribute().getHandle(), zero, NUM_ACTUATORS);
  Serial.println("Turning all off...");
}

